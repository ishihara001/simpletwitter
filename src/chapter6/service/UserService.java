//ユーザーオブジェクトを引数に取り、それをDBに登録する
package chapter6.service;

import static chapter6.utils.CloseableUtil.*;//暗号化ユーティリティ
import static chapter6.utils.DBUtil.*;//DB(コネクション関係)のユーティリティ

import java.sql.Connection;

import org.apache.commons.lang.StringUtils;//パスワードの判定のため追加

import chapter6.beans.User;
import chapter6.dao.UserDao;
import chapter6.utils.CipherUtil;

public class UserService {
	public void insert(User user) {

		Connection connection = null;
		try {
			// パスワード暗号化
			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			connection = getConnection();
			new UserDao().insert(connection, user);// 登録時のデータ引数
			commit(connection);// コミット（完全に反映）
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public User select(String accountOrEmail, String password) {

		Connection connection = null;
		try {
			// パスワード暗号化
			String encPassword = CipherUtil.encrypt(password);

			connection = getConnection();
			User user = new UserDao().select(connection, accountOrEmail, encPassword);// 参照時のデータ引数
			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	// ユーザー情報の変更機能の実装のため追加
	public User select(int userId) {

		Connection connection = null;
		try {
			connection = getConnection();
			User user = new UserDao().select(connection, userId);
			commit(connection);// コミット（完全に反映）

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	// 編集の追加
	public void update(User user) {

		Connection connection = null;
		try {
			// パスワード暗号化
			// if分岐を追加しパスワードが空だった場合素通りさせる
			if (!StringUtils.isEmpty(user.getPassword())) {
				String encPassword = CipherUtil.encrypt(user.getPassword());
				user.setPassword(encPassword);
			}
			connection = getConnection();
			new UserDao().update(connection, user);
			commit(connection);// コミット（完全に反映）
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/*
	 * String型の引数をもつ、selectメソッドを追加する
	 */
	public User select(String account) {

		Connection connection = null;
		try {
			connection = getConnection();
			User user = new UserDao().select(connection, account);
			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
