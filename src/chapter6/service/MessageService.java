package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;//メッセージ一覧を取得するために追加

import org.apache.commons.lang.StringUtils;//IDの判定のため追加

import chapter6.beans.Message;
import chapter6.beans.UserMessage;//メッセージ一覧を取得するために追加
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;//メッセージ一覧を取得するために追加

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().update(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/*
	 * selectの引数にString型のuserIdを追加
	 */
	public List<UserMessage> select(String userId, String startDate, String endDate) {
		final int LIMIT_NUM = 1000;

		Connection connection = null;
		try {
			connection = getConnection();
			/*
			 * idをnullで初期化 ServletからuserIdの値が渡ってきていたら 整数型に型変換し、idに代入
			 */
			/*
			 * messageDao.selectに引数としてInteger型のidを追加 idがnullだったら全件取得する
			 * idがnull以外だったら、その値に対応するユーザーIDの投稿を取得する
			 */
			Integer id = null;
			if (!StringUtils.isEmpty(userId)) {
				id = Integer.parseInt(userId);
			}

			Calendar newDate = Calendar.getInstance();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			String executionStartDate = null;
			String executionEndDate = null;
			if(!StringUtils.isEmpty(startDate)) {
				executionStartDate = startDate + " 00:00:00";
			} else {
				executionStartDate = "2020-01-01 00:00:00";
			}
			if(!StringUtils.isEmpty(endDate)) {
				executionEndDate = endDate + " 23:59:59";
			} else {
				executionEndDate = (format.format(newDate.getTime()));
			}

			List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, executionStartDate, executionEndDate);// つぶやきを表示するための引数
			// つぶやきを表示するための戻り値(List)
			commit(connection);// コミット（完全に反映）

			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(int messageId) {
		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().delete(connection, messageId);

			commit(connection);// コミット（完全に反映）

			return;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public Message select(int messageId) {

		Connection connection = null;
		try {
			connection = getConnection();
			Message message = new MessageDao().select(connection, messageId);
			commit(connection);// コミット（完全に反映）

			return message;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
