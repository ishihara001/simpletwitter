//ユーザー情報の変更機能の実装のため追加
package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;//編集の追加
import java.util.List;//編集の追加

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;//編集の追加

import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;//編集の追加
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");

		User user = new UserService().select(loginUser.getId());

		request.setAttribute("user", user);
		request.getRequestDispatcher("settings.jsp").forward(request, response);
	}

	@Override // 編集の追加
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		User user = getUser(request);

		if (isValid(user, errorMessages)) {
			try {
				new UserService().update(user);
			} catch (NoRowsUpdatedRuntimeException e) {
				errorMessages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
			}
		}

		new UserService().select(user.getAccount());

		if (errorMessages.size() != 0) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("user", user);
			request.getRequestDispatcher("settings.jsp").forward(request, response);
			return;
		}

		session.setAttribute("loginUser", user);
		response.sendRedirect("./");
	}

	private User getUser(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();
		user.setId(Integer.parseInt(request.getParameter("id")));
		user.setName(request.getParameter("name"));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));// 実践問題①
		user.setEmail(request.getParameter("email"));
		user.setDescription(request.getParameter("description"));
		return user;
	}

	private boolean isValid(User user, List<String> errorMessages) {

		String name = user.getName();
		String account = user.getAccount();
		String email = user.getEmail();

		User inputUser = new UserService().select(account);// UserServiceで処理して戻り値を変数に格納

		if (!StringUtils.isEmpty(name) && (20 < name.length())) {
			errorMessages.add("名前は20文字以下で入力してください");
		}
		if (StringUtils.isEmpty(account)) {
			errorMessages.add("アカウント名を入力してください");
		} else if (20 < account.length()) {
			errorMessages.add("アカウント名は20文字以下で入力してください");
		}

		if (!StringUtils.isEmpty(email) && (50 < email.length())) {
			errorMessages.add("メールアドレスは50文字以下で入力してください");
		}

		if ((inputUser != null) && (user.getId() != inputUser.getId())) {
			// getIdメソッドを使用して現在のプライマリーキーと新しいプライマリキーを参照
			errorMessages.add("アカウントが重複しています");
		}
		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
