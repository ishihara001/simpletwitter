package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;//特定のデータベースとのセッションを表現
import java.sql.PreparedStatement;//プリコンパイルされたSQL文を表すオブジェクト
import java.sql.ResultSet;
import java.sql.SQLException;//データベースアクセスエラーに関する情報を提供する例外
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Comment;//メッセージの一時保管庫
import chapter6.exception.SQLRuntimeException;

public class CommentDao {

	public void insert(Connection connection, Comment comment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");
			sql.append("    text, ");
			sql.append("    user_id, ");
			sql.append("    message_id, ");
			sql.append("    created_date, ");
			sql.append("    updated_date ");
			sql.append(") VALUES ( ");
			sql.append("    ?, "); // テキスト
			sql.append("    ?, "); // ユーザID
			sql.append("    ?, "); // メッセージID（メッセージから取得）
			sql.append("    CURRENT_TIMESTAMP, "); // created_date
			sql.append("    CURRENT_TIMESTAMP "); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, comment.getText());
			ps.setInt(2, comment.getUserId());
			ps.setInt(3, comment.getMessageId());

			ps.executeUpdate();// ユーザーとつぶやきをセットでメイクするための引数
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public Comment select(Connection connection, int commentid) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM comments WHERE id = ?";

			ps = connection.prepareStatement(sql);

			ps.setInt(1, commentid);

			ResultSet rs = ps.executeQuery();

			List<Comment> comments = toComments(rs);

			if (comments.isEmpty()) {
				return null;
			} else {
				return comments.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Comment> toComments(ResultSet rs) throws SQLException {// つぶやきを表示するための戻り値

		List<Comment> comments = new ArrayList<Comment>();
		try {
			while (rs.next()) {
				Comment comment = new Comment();
				comment.setId(rs.getInt("id"));
				comment.setUserId(rs.getInt("user_id"));
				comment.setText(rs.getString("text"));
				comment.setCreatedDate(rs.getTimestamp("created_date"));
				comment.setUpdatedDate(rs.getTimestamp("updated_date"));

				comments.add(comment);
			}
			return comments;
		} finally {
			close(rs);
		}
	}
}
