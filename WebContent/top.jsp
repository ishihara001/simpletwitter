<!-- トップ画面のJSP -->
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- @page cssとの連携 -->
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- ログイン初期 -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>簡易Twitter</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<!-- CSSを読み込むコード -->
</head>
<body>
	<div class="main-contents">

		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<a href="signup">登録する</a>
			</c:if>
			<!-- 「ホーム」「設定」「ログアウト」を表示 -->
			<c:if test="${ not empty loginUser }">
				<a href="./">ホーム</a>
				<a href="setting">設定</a>
				<a href="logout">ログアウト</a>
			</c:if>
		</div>

		<!-- ログインユーザーの情報を表示させるためのコード -->
		<c:if test="${ not empty loginUser }">
			<div class="profile">
				<div class="name">
					<h2>
						<c:out value="${loginUser.name}" />
					</h2>
				</div>
				<div class="account">
					@
					<c:out value="${loginUser.account}" />
				</div>
				<div class="description">
					<c:out value="${loginUser.description}" />
				</div>
			</div>
		</c:if>

		<!-- エラー表示 -->
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<!-- 絞り込み用フォームの作成 -->
		<form action="./" method="get">
			<input name="startDate" type="date" value="${startDate}">
				～
			<input name="endDate" type="date" value="${endDate}">
			<input type="submit" value="絞り込み">
		</form>

		<!-- メッセージのつぶやき機能を実装 -->
		<div class="form-area">
			<c:if test="${ isShowMessageForm }">
				<form action="message" method="post">
					いま、どうしてる？<br />
					<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
					<br /> <input type="submit" value="つぶやく">（140文字まで）
				</form>
			</c:if>
		</div>

		<!--メッセージを表示するコード-->
		<div class="messages">
			<c:forEach items="${messages}" var="message">
				<div class="message">
					<div class="account-name">
						<span class="account"> <!-- ボタンの追加 -->
						<a href="./?user_id=<c:out value="${message.userId}"/> "><br />
							<c:out value="${message.account}" />
						</a>

						</span> <span class="name"><c:out value="${message.name}" /></span>
					</div>
					<div class="text">
						<pre><c:out value="${message.text}" /></pre>
					</div>

					<c:if test="${loginUser.id == message.userId}">
						<form action="edit" method="get">
							<input type="submit" value="編集"><br />
							<input name="messageId" value="${message.id}" id="messageId" type="hidden" />
						</form>
					<!-- 編集ボタンの追加  削除ボタンの追加 -->
						<form action="deleteMessage" method="post">
							<input type="submit" value="削除"><br />
							<input name="messageId" value="${message.id}" id="messageId" type="hidden" />
						</form>
					</c:if>
					<div class="date">
						<fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
					</div>

					<!-- 返信メッセージの表示 -->
					<c:forEach items="${comments}" var="comment">
						<c:if test="${message.id == comment.messageId}">
							<div class="message">
								<div class="account-name">
									<span class="account">
									<c:out value="${comment.account}"/>
									</span> <span class="name"><c:out value="${comment.name}" /></span>
								</div>
								<div class="text">
									<pre><c:out value="${comment.text}" /></pre>
								</div>
								<div class="date">
									<fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
								</div>
							</div>
						</c:if>
					</c:forEach>

					<c:if test="${ isShowMessageForm }"><!-- 返信メッセージ機能を実装 -->
						<form action="comment" method="post">
							<textarea name="commentText" cols="100" rows="5" class="tweet-box"></textarea>
							<br /> <input type="submit" value="返信">（140文字まで）
							<input name="messageId" value="${message.id}" id="messageId" type="hidden" />
						</form>
					</c:if>
				</div>
			</c:forEach>
		</div>
		<div class="copyright">Copyright(c)IshiharaYamato</div>
	</div>
</body>
</html>